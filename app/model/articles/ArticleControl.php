<?php

namespace App\Model\Articles;

use App\Model\Constants\Article;
use Ublaboo\DataGrid\DataGrid;
use Nette;

class ArticleControl extends \Nette\Application\UI\Control
{
    /**
     * @var Nette\Database\Context
     */
    public $db;
    /**
     * @var Nette\Security\User
     */
    public $user;
    /**
     * @var Nette\Http\Request
     */
    public $request;


    public function __construct(Nette\Database\Context $db, Nette\Security\User $user, Nette\Http\Request $request)
    {
        $this->db = $db;
        $this->user = $user;
        $this->request = $request;
    }


    public function render(): void
    {
        $this->template->render(__DIR__ . '/article_control.latte');
    }


    public function createComponentSimpleGrid($name): void
    {
        $grid = new DataGrid($this, $name);

        $grid->setDataSource($this->getDataSource());
        $grid->addColumnText('title', 'Nadpis')
            ->setSortable();
        $grid->addColumnNumber('rating', 'Hodnocení')
            ->setSortable();
        $grid->addColumnDateTime('created', 'Vloženo')
            ->setFormat('d.m.Y H:i:s')
            ->setSortable();
        if($this->user->isLoggedIn()) {
            $grid->addAction('like', 'Like', 'likeArticle!')
                ->setIcon('thumbs-up')
                ->setClass('btn btn-xs btn-success ajax');
            $grid->addAction('unlike', 'Unlike', 'unlikeArticle!')
                ->setIcon('thumbs-down')
                ->setClass('btn btn-xs btn-danger ajax');
        }
//        $grid->addAction('unlike', 'Unlike', '!unlikeArticle');
    }

    protected function getDataSource(): Nette\Database\Table\Selection
    {
        $db = $this->db->table('article')->select('*');
        if(!$this->user->isLoggedIn()){
            $db->where('visible', Article::VISIBLE_ALL);
        }
        return $db;
    }

    protected function processResult($id): void
    {
        if ($this->request->isAjax()) {
            $this->redrawControl('flashes');
            $this['actionsGrid']->redrawItem($id);
        } else {
            $this->redirect('this');
        }
    }

    public function handleLikeArticle($id): void
    {
        $this->updateRating((int)$id, true);
        $this->processResult($id);
    }

    public function handleUnlikeArticle($id): void
    {
        $this->updateRating((int)$id, false);
        $this->processResult($id);
    }

    protected function updateRating(int $articleId, bool $increase = true): void
    {
        $value = (int) $increase;
        try {
            $rating = $this->db->table('rating')->insert([
                'article_id' => $articleId,
                'user_id' => $this->user->getId(),
                'like' => $value
            ]);
        } catch (Nette\Database\UniqueConstraintViolationException $e) {
            if(($e->getCode() == 23000) && (isset($e->errorInfo[1]) && $e->errorInfo[1] == 1062)){
                $rating = $this->db->table('rating')
                    ->where('article_id', $articleId)
                    ->where('user_id', $this->user->getId())
                    ->update([
                        'article_id' => $articleId,
                        'user_id' => $this->user->getId(),
                        'like' => $value
                    ]);
            } else {
                throw $e;
            }
        }
        $ratingLoad = $this->db->table('rating')->select('SUM(like) AS likes, COUNT(*) AS amount')->where('article_id', $articleId)->fetch();
        $article = $this->db->table('article')->where('id', $articleId)->update([
            'likes' => $ratingLoad->likes,
            'likes_total' => $ratingLoad->amount,
            'rating' => round($ratingLoad->likes / $ratingLoad->amount, 4) * 100
        ]);

    }
}