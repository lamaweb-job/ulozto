<?php

namespace App\Model\Articles;

interface IArticleControlControlFactory
{

    /**
     * @return ArticleControl
     */
    function create();

}