<?php

require __DIR__ . '/../vendor/autoload.php';
require_once(CONFIG_DIR . '/config.php');

$configurator = new Nette\Configurator;
\Tracy\Debugger::$showLocation = true;
$configurator->setDebugMode(true); //'23.75.345.200'); // enable for your remote IP
$configurator->enableTracy(__DIR__ . '/../log');

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$configurator->addConfig(__DIR__ . '/config/config.neon');
$configurator->addConfig(__DIR__ . '/config/config.local.neon');

$container = $configurator->createContainer();

return $container;
