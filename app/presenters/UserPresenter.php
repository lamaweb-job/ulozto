<?php

namespace App\Presenters;

use app\forms\User\ChangePassword;
use Nette;
use Nette\Application\UI\Form;


final class UserPresenter extends BasePresenter
{
    /**
     * @var Nette\Database\Context
     * @inject
     */
    public $db;

    public function actionChangePassword(): void
    {
        if(!$this->getUser()->isLoggedIn()){
            $this->flashMessage('You must be registered');
            $this->redirect('Homepage:');
        }
    }

    protected function createComponentPasswordChangeForm(): Form
    {
        $form = (new ChangePassword())->create();
        $form->onSuccess[] = [$this, 'passwordChangeSubmitted'];
        return $form;
    }

    public function passwordChangeSubmitted(Form $form): void
    {
        $values = $form->getValues();
        $password = $this->db->table('user')->select('password')->where('id', $this->getUser()->getId())->fetch();
        $isVerified = Nette\Security\Passwords::verify($values->passwordOld, $password->password);
        if(!$isVerified){
            $this->flashMessage('Špatné původní heslo', FLASH_MESSAGE_ERROR);
        } else {
            $this->flashMessage('Heslo bylo úspěšně změněno.', FLASH_MESSAGE_SUCCESS);
            $this->db->table('user')->where('id', $this->getUser()->getId())->update(['password' => Nette\Security\Passwords::hash($values->password)]);
        }
        $this->redirect('this');

    }
}
