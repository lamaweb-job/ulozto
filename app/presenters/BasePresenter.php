<?php

namespace App\Presenters;

use app\forms\User\Login;
use Nette;
use Nette\Application\UI\Form AS Form;
use Ublaboo\DataGrid\DataGrid;


abstract class BasePresenter extends Nette\Application\UI\Presenter
{

    protected function createComponentLoginForm(): Form
    {
        $form = (new Login())->create();
        $form->onSuccess[] = [$this, 'loginFormSubmitted'];

        return $form;
    }

    public function loginFormSubmitted(Form $form): void
    {
        $values = $form->getValues();
        try {
            $this->getUser()->login($values->username, $values->password);
            $this->getUser()->setExpiration(0, TRUE);
            $this->flashMessage('Log in successful', FLASH_MESSAGE_SUCCESS);
            $this->redirect('Homepage:');
        } catch(\Nette\Security\AuthenticationException $e) {
            $form->addError($e->getMessage());
        }
    }

    public function handleLogout(): void
    {
        $this->getUser()->logout(true);
        $this->flashMessage('Logout successful!', FLASH_MESSAGE_SUCCESS);
        $this->redirect('this');
    }
}
