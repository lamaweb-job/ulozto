<?php

namespace App\Presenters;

use app\forms\User\Registration;
use Nette;
use Nette\Application\UI\Form;


final class RegistrationPresenter extends BasePresenter
{
    /**
     * @var Nette\Database\Context
     * @inject
     */
    public $db;

    public function actionDefault(): void
    {
        if($this->getUser()->isLoggedIn()){
            $this->flashMessage('You can not be registered');
            $this->redirect('Homepage:');
        }
    }

    protected function createComponentRegistrationForm(): Form
    {
        $form = (new Registration())->create();
        $form->onSuccess[] = [$this, 'loginFormSubmitted'];

        return $form;
    }

    public function loginFormSubmitted(Form $form): void
    {
        $values = $form->getValues();
        $this->db->table('user')->insert([
            'username' => $values->username,
            'password' => Nette\Security\Passwords::hash($values->password),
            'role' => 'user',
            'registered' => new \DateTime(),
        ]);
        $this->getUser()->login($values->username, $values->password);
        $this->redirect('Homepage:');
    }
}
