<?php

namespace App\Presenters;

use App\Model\Articles\ArticleControl;


final class HomepagePresenter extends BasePresenter
{
    public function renderDefault(): void
    {

    }


    /**
     * @var \App\Model\Articles\IArticleControlControlFactory
     * @inject
     */
    public $articleControlFactory;


    public function createComponentArticleControl(): ArticleControl
    {
        return $this->articleControlFactory->create();
    }
}
