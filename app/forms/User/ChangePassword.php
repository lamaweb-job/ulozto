<?php

namespace app\forms\User;

use Nette\Application\UI\Form;

class ChangePassword
{
    const PASSWORD_MIN_LENGTH = 3;

    /**
     * @return Form
     */
    public function create(): Form
    {
        $form = new Form;

        $form->addPassword('passwordOld', 'Staré heslo:')
            ->setRequired()
            ->addRule(Form::FILLED, 'Musíte zadat staré heslo');
        $form->addPassword('password', 'Heslo:')
            ->setRequired()
            ->addRule(Form::MIN_LENGTH, 'Heslo musí být alespoň '.Registration::PASSWORD_MIN_LENGTH.' znaky dlouhé', Registration::PASSWORD_MIN_LENGTH);
        $form->addPassword('password2', 'Znovu Heslo:')
            ->setRequired()
            ->addRule(Form::EQUAL, 'Hesla se neshodují', $form['password']);
        $form->addSubmit('change', 'Změnit heslo');

        return $form;
    }
}