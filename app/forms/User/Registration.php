<?php

namespace app\forms\User;

use Nette\Application\UI\Form;

class Registration
{
    const PASSWORD_MIN_LENGTH = 3;
    /**
     * @return Form
     */
    public function create(): Form
    {
        $form = new Form;

        $form->addText('username', 'Jméno:')
            ->setRequired()
            ->addRule(Form::MIN_LENGTH, 'Jméno musí být alespoň '.self::PASSWORD_MIN_LENGTH.' znaky dlouhé', self::PASSWORD_MIN_LENGTH);
        $form->addPassword('password', 'Heslo:')
            ->setRequired()
            ->addRule(Form::MIN_LENGTH, 'Heslo musí být alespoň '.self::PASSWORD_MIN_LENGTH.' znaky dlouhé', self::PASSWORD_MIN_LENGTH);
        $form->addPassword('password2', 'Znovu Heslo:')
            ->setRequired()
            ->addRule(Form::EQUAL, 'Hesla se neshodují', $form['password']);
        $form->addHidden('email');
        $form->addSubmit('login', 'Registrovat se');

        return $form;
    }
}