<?php

namespace app\forms\User;

use Nette\Application\UI\Form;

class Login
{
    /**
     * @return Form
     */
    public function create(): Form
    {
        $form = new Form;

        $form->addText('username', 'Jméno:');
        $form->addPassword('password', 'Heslo:');
        $form->addHidden('email');
        $form->addSubmit('login', 'Přihlásit se');

        return $form;
    }
}