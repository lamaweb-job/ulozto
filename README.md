Job Task
---

## Zadání

Níže vkládám popis drobného úkolu, jak jsme se bavili, cílem není mít vše na 100 % do nejmenších detailů.
Jde jen o denmonstraci kombinace PHP + Nette + lehký přesah do infrastruktury.
Zda použijete Vagrant a Puppet nebo Docker necháme na vás.

### Obecné zadání

Vytvořte jednoduchou PHP + Nette + MySQL aplikaci vyvíjenou na prostředí Vagrant + VirtualBox.  
Hotový úkol odevzdejte jako jeden GIT repositář na github.com.  
Vagrant provisioning by měl být zpracovaný tak, aby aplikace byla po nastartování pokud možno funkční.  
Popište postup potřebný k nastartování a zobrazení aplikace v browseru do readme souboru v rootu repositáře.  
Co není přímo zadáno (struktura repositáře, coding style, validace, ošetření chybových stavů a pod.) udělejte podle svého nejlepšího uvážení.  
Pokud se rozhodnete něco zjednodušit nebo vynechat kvůli nepřiměřenému rozsahu práce, napište alespoň zmínku do readme, jak byste to považovali za správné ve skutečné aplikaci.  

### Požadavky na virtuální stroj
použít Vagrant a VirtualBox (na základě domluvy použit pouze docker)  
Linux distribuce Debian nebo Ubuntu (na základě domluvy použit pouze docker)  
nainstalované aplikace (na základě domluvy použit pouze docker)  
    Nginx  
        name-based vhost pojemenovaný "nubium-sandbox.test"  
        porty: 80 HTTP a 443 HTTPS  
    PHP 7.1 nebo vyšší  
    MySQL 5.7 nebo vyšší  
        sql-mode musí obsahovat STRICT_ALL_TABLES  
Můžete si pomoct použitím https://puphpet.com/ nebo podobným nástrojem dle Vašich preferencí.  

### Požadavky na PHP aplikaci
Nette 2.4  
SQL layer Dibi nebo Nette database  
všechny použité PHP knihovny instalovat pomoci Composeru  
aplikace musí umožňovat  
    registraci, přihlášení a změnu hesla uživatele  
    výpis "článků" umožňující  
        stránkování  
        řazení podle data vložení, nadpisu, hodnocení  
        omezení zobrazení jednotlivých článků pouze přihlášeným  
         přihlášený může hodnotit tlačítkem like/dislike (+1/-1) bez reloadu celé stránky  
    bez administrace (stačí SQL soubor pro počáteční naplnění)  
minimální DB struktura  
    uživatel: login, heslo, informace o registraci  
    článek: nadpis, perex, datum vystavení, informace o viditelnosti a hodnocení  
    uložené informace musí umožnit smazat hodnocení provedená nepoctivým uživatelem  
vzhled a JavaScripty musí fungovat v posledních verzích IE, Edge, Chrome, Firefox.  
    
## spustění
### docker kontejnery
vytvoření všech docker containerů
```
docker-compose up --build -d
```
### struktura DB 
pro vytvoření základní struktury databáze je nejprve nutné se přihlísit do kontejneru s DB
```
docker-compose exec uloz-db bash
```
a pak je potřeba spustit skript na vytvoření DB
```
mysql -uroot -puloz < /var/install/install.sql && exit
```

## Splněné a nesplněné části
Celkově jsem po předchozí domluvě využil Docker místo Vagrantu. Docker využívá MariaDb, PHP 7.2, Nginx a Nginx proxy (na ni lze napojit let's encrypt docker pro https.  

Aplikace využívá zadané parametry a splňuje zadání.  
Jediné co jsem vynechal, je využití AJAX z důvodu, že jsem nebyl v krátké době schopen nastavit přijímání tohoto zasílání.
Ještě jedna změna je využití GitLab místo GitHubu.

Hesla k jednotlivým uživatelům, jsou shodná s jejich `username`.