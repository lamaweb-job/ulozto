CREATE DATABASE `uloz` COLLATE 'utf8_unicode_ci';

USE uloz;

-- Adminer 4.7.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `perex` text COLLATE utf8_unicode_ci NOT NULL,
  `likes` int(11) NOT NULL,
  `likes_total` int(11) NOT NULL,
  `rating` decimal(5,2) NOT NULL,
  `visible` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `article_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
CREATE TABLE `rating` (
  `article_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `like` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`article_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registered` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `article` (`id`, `title`, `perex`, `likes`, `likes_total`, `rating`, `visible`, `user_id`, `created`) VALUES
(1,	'Prvni clanek',	'Tohle je uvod k prvnimu clanku',	1,	2,	50.00,	1,	1,	'2018-01-02 06:07:08'),
(2,	'Druhy clanek',	'Tohle je uvod k druhemu clanku',	0,	1,	0.00,	0,	1,	'2019-01-26 15:12:08'),
(3,	'Treti clanek',	'perex ke tretimu clanku',	0,	0,	0.00,	1,	2,	'2019-01-26 21:28:52'),
(4,	'4 clanek',	'perex ke 4. clanku',	0,	0,	0.00,	0,	2,	'2019-01-26 21:35:17'),
(5,	'5 clanek',	'perex ke 5. clanku',	0,	0,	0.00,	1,	1,	'2019-01-26 21:35:17'),
(6,	'6 clanek',	'perex ke 6. clanku',	1,	1,	100.00,	0,	2,	'2019-01-26 21:35:17'),
(7,	'7 clanek',	'perex ke 7. clanku',	0,	0,	0.00,	1,	3,	'2019-01-26 21:35:17'),
(8,	'8 clanek',	'perex ke 8. clanku',	0,	0,	0.00,	0,	4,	'2019-01-26 21:35:17'),
(9,	'9 clanek',	'perex ke 9. clanku',	0,	0,	0.00,	1,	1,	'2019-01-26 21:35:17'),
(10,	'10 clanek',	'perex ke 10. clanku',	0,	0,	0.00,	0,	4,	'2019-01-26 21:35:17'),
(11,	'11 clanek',	'perex ke 11. clanku',	0,	0,	0.00,	1,	2,	'2019-01-26 21:35:17'),
(12,	'12 clanek',	'perex ke 12. clanku',	0,	0,	0.00,	0,	3,	'2019-01-26 21:35:17'),
(13,	'13 clanek',	'perex ke 13. clanku',	0,	0,	0.00,	1,	3,	'2019-01-26 21:35:17'),
(14,	'14 clanek',	'perex ke 14. clanku',	0,	0,	0.00,	0,	1,	'2019-01-26 21:35:17'),
(15,	'15 clanek',	'perex ke 15. clanku',	0,	0,	0.00,	1,	4,	'2019-01-26 21:35:17'),
(16,	'16 clanek',	'perex ke 16. clanku',	0,	0,	0.00,	0,	2,	'2019-01-26 21:35:17'),
(17,	'17 clanek',	'perex ke 17. clanku',	0,	0,	0.00,	1,	2,	'2019-01-26 21:35:17'),
(18,	'18 clanek',	'perex ke 18. clanku',	0,	0,	0.00,	0,	3,	'2019-01-26 21:35:17'),
(19,	'19 clanek',	'perex ke 19. clanku',	0,	0,	0.00,	1,	1,	'2019-01-26 21:35:17'),
(20,	'20 clanek',	'perex ke 20. clanku',	0,	0,	0.00,	0,	1,	'2019-01-26 21:35:17'),
(21,	'21 clanek',	'perex ke 21. clanku',	0,	0,	0.00,	1,	1,	'2019-01-26 21:35:17'),
(22,	'22 clanek',	'perex ke 22. clanku',	0,	0,	0.00,	0,	2,	'2019-01-26 21:35:17'),
(23,	'23 clanek',	'perex ke 23. clanku',	0,	0,	0.00,	1,	2,	'2019-01-26 21:35:17'),
(24,	'24 clanek',	'perex ke 24. clanku',	0,	0,	0.00,	0,	2,	'2019-01-26 21:35:17'),
(25,	'25 clanek',	'perex ke 25. clanku',	0,	0,	0.00,	0,	2,	'2019-01-26 21:35:17'),
(26,	'26 clanek',	'perex ke 26. clanku',	0,	0,	0.00,	0,	2,	'2019-01-26 21:35:17'),
(27,	'27 clanek',	'perex ke 27. clanku',	0,	0,	0.00,	0,	2,	'2019-01-26 21:35:17'),
(28,	'28 clanek',	'perex ke 28. clanku',	0,	0,	0.00,	0,	2,	'2019-01-26 21:35:17'),
(29,	'29 clanek',	'perex ke 29. clanku',	0,	0,	0.00,	0,	2,	'2019-01-26 21:35:17');

INSERT INTO `rating` (`article_id`, `user_id`, `like`) VALUES
(1,	1,	1),
(1,	6,	0),
(2,	1,	0),
(6,	6,	1);

INSERT INTO `user` (`id`, `username`, `password`, `role`, `registered`) VALUES
(1,	'test',	'$2y$10$vwku.pfhHoqz0A8yhNRAB./hc1XFWcg.WOCPbqQFOijuVphWfUd4O',	'user',	'2016-01-02 03:04:05'),
(2,	'test2',	'$2y$10$9rc8aKnwhXnNyLpRdEZrSu84E0H9wmEvQr3JeTHLh1HtSJ7EQb4HK',	'user',	'2019-01-26 18:32:14'),
(3,	'test3',	'$2y$10$zLWSB94VqOTQjALkUU7W1ec1NfDl23.THPIwEMWI35EwuY7/2xVea',	'user',	'2019-01-26 18:32:30'),
(4,	'test4',	'$2y$10$8nOtmcgUtD3CBVj4nI3C3uzDbpuEY0Wt2wXSy9mt2nFJw7L9OK1w2',	'user',	'2019-01-26 18:32:30'),
(5,	'test5',	'$2y$10$WEVn2Q3/1tC/dvKMVCpm/Ok9KwgKRW0aggv8a/djjXRIrRb5dD82C',	'user',	'2019-01-27 01:47:55'),
(6,	'test6',	'$2y$10$y8nAl5b.uqiEjoiOb6D3fee92tiV3hg1cVeFoIYiScEPwWXo2Ia5.',	'user',	'2019-01-27 01:48:51'),
(7,	'test7',	'$2y$10$0.zEUuXk6QRKcWdO/lCWw.yDVJ5phQIs3BidcSrYyHGnoFPsjrCHS',	'user',	'2019-01-27 02:21:54');

-- 2019-01-27 10:46:10